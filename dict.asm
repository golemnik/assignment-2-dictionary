%include "lib.inc"
%include "colon.inc"

global find_word

section .text

; rdi - word
; rsi - dictionary
find_word:
    push r12
    push r13

.next:
    cmp rsi, 0
    je .not_found
    mov r12, rsi
    mov r13, rdi

    add rsi, DICTIONARY_ITEM_HEADER_SIZE

    call string_equals
    mov rsi, r12
    mov rdi, r13
    cmp ax, 0
    jne .found
    mov rsi, [rsi]
    jmp .next

.not_found:
    xor rax, rax
    jmp .done

.found:
    mov rax, rsi

.done:
    pop r13
    pop r14
    ret
