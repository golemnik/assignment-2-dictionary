%include "words.inc"
%include "dict.inc"
%include "lib.inc"
%include "main.inc"

global _start

section .text
_start:
    call main
    mov rdi, rax
    call exit
