%define DICTIONARY_ITEM_HEADER_SIZE 8 * 2
%define DICTIONARY_ITEM_DEFINITION_OFFSET 8

%define next_colon 0
%macro colon 2
%2:
; ----------------------------------- header begin
	dq	next_colon
	dq	%%colon_definition
; ----------------------------------- header end
	db	%1, 0
%%colon_definition:
%define next_colon %2
%endmacro
