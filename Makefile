all: dictionary test

test:
	/usr/bin/python3 tests.py

dictionary: main.o dict.o lib.o
	ld -o $@ $^

clean:
	rm -f *.o *.lst dictionary report.xml test_*
	rm -rf __pycache__


.PHONY: all test clean

NASM_OPTS=-f elf64

lib.o: lib.asm lib.inc
	nasm ${NASM_OPTS} $< -o $@

dict.o: dict.asm dict.inc
	nasm ${NASM_OPTS} $< -o $@

main.o: main.asm main.inc lib.inc dict.inc words.inc colon.inc
	nasm ${NASM_OPTS} -l $@.lst $< -o $@
