#!/usr/bin/python3

__unittest = True

import subprocess
import re
import unittest
import xmlrunner
from subprocess import CalledProcessError, Popen, PIPE


READ_FAILURE_RC = 1
READ_FAILURE_MSG = "Error while reading a word"

NOT_FOUND_RC = 2
NOT_FOUND_MSG = "Not found"

class DictionaryItem:
    def __init__(self, label, word, definition):
        self.label = label
        self.word = word
        self.definition = definition


class DictionaryTest(unittest.TestCase):
    not_compiled = True

    def _compile_asm(self, fname):
        self.assertEqual(subprocess.call( ['nasm', '-l', fname + '.lst', '-Le', '-f', 'elf64', fname + '.asm', '-o', fname+'.o'] ), 0, 'failed to compile')

    def _compile_libs(self):
        if self.not_compiled:
            self._compile_asm("lib")
            self._compile_asm("dict")
            self.not_compiled = False

    def _compile(self, fname, text):
        f = open( fname + '.asm', 'w')
        f.write( text )
        f.close()

        self.assertEqual(subprocess.call( ['nasm', '-l', fname + '.lst', '-Le', '-f', 'elf64', fname + '.asm', '-o', fname+'.o'] ), 0, 'failed to compile')
        self.assertEqual(subprocess.call( ['ld', '-o' , fname, fname+'.o', 'lib.o', 'dict.o'] ), 0, 'failed to link')

    def launch(self, fname, input):
        output = b''
        try:
            p = Popen(['./'+fname], shell=None, stdin=PIPE, stdout=PIPE, stderr=PIPE)
            (output, errors) = p.communicate(input.encode())
            self.assertNotEqual(p.returncode, -11, 'segmentation fault')
            return (errors.decode(), output.decode(), p.returncode)
        except CalledProcessError as exc:
            self.assertNotEqual(exc.returncode, -11, 'segmentation fault')
            return (exc.output.decode(), exc.returncode)

    def perform(self, fname, text, input):
        self._compile_libs()
        self._compile(fname, self.before_all + text)
        return self.launch(fname, input)

    before_all = """
%include "colon.inc"
%include "dict.inc"
%include "lib.inc"
%include "main.inc"

global _start

section .text
_start:
    call main
    mov rdi, rax
    call exit
"""

    def generate_dictionary_text(self, items: list[DictionaryItem]):
        first_word_label = "0"
        text_items = "";
        for item in items:
            first_word_label = ".%s" % item.label
            text_items += """
                    colon "%s", .%s
                    db "%s", 0
            """ % (item.word, item.label, item.definition)

        text = """
               section .rodata
               dictionary: dq %s
               %s
               dictionary_end:
        """ % (first_word_label, text_items)
        return text


    def test_empty_dictionary(self):
        dictionary = self.generate_dictionary_text( [] )
        input = "any word"
        (errors, output, code) = self.perform("test_empty_dictionary", dictionary, input)
        self.assertEqual(NOT_FOUND_RC, code, 'Unexpected exit code: expected %s, actual %s' % (NOT_FOUND_RC, code))
        self.assertEqual(NOT_FOUND_MSG, errors.strip(), 'Unexpected error message: %s' % repr(errors))
        self.assertEqual('', output.strip(), 'Unexpected output: %s' % repr(output))


    def test_one_word(self):
        dictionary = self.generate_dictionary_text([
            DictionaryItem("one_word", "one word", "definition for one word")
        ])
        input = "one word"
        (errors, output, code) = self.perform("test_one_word", dictionary, input)
        self.assertEqual(0, code, 'Unexpected exit code: expected %s, actual %s' % (NOT_FOUND_RC, code))
        self.assertEqual('', errors.strip(), 'Unexpected error message: %s' % repr(errors))
        self.assertEqual('definition for one word', output.strip(), 'Unexpected output: %s' % repr(output))

    def test_empty_word(self):
        dictionary = self.generate_dictionary_text([
            DictionaryItem("one_word", "", "definition for empty word")
        ])
        input = ""
        (errors, output, code) = self.perform("test_empty_word", dictionary, input)
        self.assertEqual(0, code, 'Unexpected exit code: expected %s, actual %s' % (NOT_FOUND_RC, code))
        self.assertEqual('', errors.strip(), 'Unexpected error message: %s' % repr(errors))
        self.assertEqual('definition for empty word', output.strip(), 'Unexpected output: %s' % repr(output))



    def test_real_found(self):
        items = (
            DictionaryItem(None, 'first word',  'first word explanation'),
            DictionaryItem(None, ' second word', 'second word explanation'),
            DictionaryItem(None, 'third word\n',  'third word explanation'),
        )
        for item in items:
            (errors, output, code) = self.launch("dictionary", item.word)
            self.assertEqual(0, code, "word=%s code=%s output=%s errors=%s" % (repr(item.word), code, output, errors))
            self.assertEqual('', errors.strip(), "code=%s output=%s errors=%s" % (code, output, errors))
            self.assertEqual(item.definition, output.strip(), "code=%s output=%s errors=%s" % (code, output, errors))


    def test_real_not_found(self):
        words = ('first worD', 'second  word', 'third', 'w' * 255)
        for word in words:
            (errors, output, code) = self.launch("dictionary", word)
            self.assertEqual(NOT_FOUND_RC, code, "word=%s code=%s output=%s errors=%s" % (word, code, output, errors))
            self.assertEqual(NOT_FOUND_MSG, errors.strip(), "word=%s code=%s output=%s errors=%s" % (word, code, output, errors))
            self.assertEqual('', output.strip(), "word=%s code=%s output=%s errors=%s" % (word, code, output, errors))

    def test_real_read_failure(self):
        words = ('x' * 256, 'y' * 1024)
        for word in words:
            (errors, output, code) = self.launch("dictionary", word)
            self.assertEqual(READ_FAILURE_RC, code, "word=%s code=%s output=%s errors=%s" % (repr(word), code, output, errors))
            self.assertEqual(READ_FAILURE_MSG, errors.strip(), "code=%s output=%s errors=%s" % (code, output, errors))
            self.assertEqual('', output.strip(), "code=%s output=%s errors=%s" % (code, output, errors))



if __name__ == "__main__":
    with open('report.xml', 'w') as report:
        unittest.main(testRunner=xmlrunner.XMLTestRunner(output=report), failfast=False, buffer=False, catchbreak=False)
