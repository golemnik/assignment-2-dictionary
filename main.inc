section .data

BUFFER_LEN equ 256
buffer: times 256 db 0

section .rodata

READ_ERROR_CODE equ 1
FIND_ERROR_CODE equ 2

read_error: db 'Error while reading a word', 0
find_error: db 'Not found',0

section .text

STDOUT equ 1
STDERR equ 2


main:
    mov rdi, buffer
    mov rsi, BUFFER_LEN
    call read_string
    or rax,rax
    jne .find
    mov rdi,read_error
    mov rsi, STDERR
    mov rax, READ_ERROR_CODE
    jmp .print
.find:
    mov rdi, rax
    mov rsi, [dictionary]
    call find_word
    or rax,rax
    jne .found
    mov rdi,find_error
    mov rsi, STDERR
    mov rax, FIND_ERROR_CODE
    jmp .print
.found:
    mov rdi,[rax + DICTIONARY_ITEM_DEFINITION_OFFSET]
    mov rsi, STDOUT
    xor rax,rax
.print:
    push rax
    push rsi
    call print_string2
    pop rdi
    call print_newline2
    pop rax
.done:
    ret
