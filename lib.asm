global exit
global print_string2
global print_newline2
global string_equals
global read_word
global read_string

section .data
buf:  db 0

STDIN  equ 0
STDOUT equ 1

SYSCALL_READ  equ 0
SYSCALL_WRITE equ 1
SYSCALL_EXIT  equ 60

section .text

; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYSCALL_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    push rcx
    mov rax, 0
    mov cl, al
.loop:
    cmp [rdi], cl
    je .done
    inc rdi
    inc rax
    jmp .loop
.done:
    pop rcx
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
; rdi = string ptr
print_string:
    mov rsi, STDOUT
    jmp print_string

; rdi = string ptr
; rsi = STDOUT/STDERR
print_string2:
    push rdx

    push rdi
    push rsi
    call string_length
    mov rdx, rax
    pop rdi
    pop rsi
    mov rax, SYSCALL_WRITE
    syscall

    pop rdx
    ret

; Принимает код символа и выводит его в stdout
; rdi
print_char:
    mov rsi, STDOUT
    jmp print_char

; rdi = char 
; rsi = STDOUT/STDERR
print_char2:
    push rdx

    mov ax, di
    mov rdi, rsi
    mov rsi, buf
    mov [rsi], al
    mov rdx, 1
    mov rax, SYSCALL_WRITE
    syscall

    pop rdx
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char

; rdi = STDOUT/STDERR
print_newline2:
    mov rsi, rdi
    mov rdi, 0xA
    jmp print_char2

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
; rdi
print_uint:
    xor rdx,rdx
    mov rcx, 10
.print_rest_num:
    cmp rdi, rcx
    jae .next
.print_digit:
    add rdi, '0'
    call print_char
    ret
.next:
    mov rax, rdi
    div rcx
    mov r10, rax
    mul rcx
    sub rdi, rax
    push di
    mov rdi, r10
    call .print_rest_num
    pop di
    jmp .print_digit

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jge print_uint
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
; rdi
; rsi
string_equals:
.loop:
    mov al, [rdi]
    mov ah, [rsi]
    cmp al, ah
    jne .not_equal
    cmp al, 0
    je  .equal
    inc rdi
    inc rsi
    jmp .loop
.not_equal:
    mov rax, 0
    ret
.equal:
    mov rax, 1
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
;;    push rdi
;;    push rsi
    push rdx

    mov rsi, buf
    mov rdx, 1
    mov rdi, STDIN
    mov rax, SYSCALL_READ
    syscall
    cmp rax, 0
    jle .eof_or_error
    mov al, [rsi]
    jmp .done
.eof_or_error:
    xor rax, rax
.done:
    pop rdx
;;    pop rsi
;;    pop rdi
    ret


%macro read_char_and_check 2
    call read_char
    cmp rax, 0
    je %1
    cmp al, 0x20
    je %2
    cmp al, 0x9
    je %2
    cmp al, 0xA
    je %2
%endmacro

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
; rdi - buf
; rsi - len
read_word:
    push r12
    push r13

    push rsi
    push rdi

    mov r12, rdi
    mov r13, rsi
.skip:
    read_char_and_check .eof, .skip
.loop:
    dec r13
    cmp r13, 0
    je .overflow
    mov [r12], al
    inc r12
    read_char_and_check .eof, .eow
    jmp .loop
.eow:
    xor al,al
.eof:
    mov [r12], al
    pop rax
    pop rdx
    sub rdx, r13
    jmp .done
.overflow:
    pop rax
    xor rax, rax
    pop rdx
.done:
    pop r13
    pop r12
    ret

read_string:
    push r12
    push r13

    push rsi
    push rdi

    mov r12, rdi
    mov r13, rsi
.skip:
    read_char_and_check .eof, .skip
.loop:
    dec r13
    cmp r13, 0
    je .overflow
    mov [r12], al
    inc r12
    call read_char
    cmp rax, 0
    je .eof
    cmp al, 0xA
    je .eol
    jmp .loop
.eol:
    xor al,al
.eof:
    mov [r12], al
    pop rax
    pop rdx
    sub rdx, r13
    jmp .done
.overflow:
    pop rax
    xor rax, rax
    pop rdx
.done:
    pop r13
    pop r12
    ret


 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
; rdi
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor rsi, rsi
    xor rcx, rcx
    mov r8, 10
.loop:
    mov cl, [rdi]
    cmp cl, 0
    je .done
    cmp cl, '0'
    jb .done
    cmp cl, '9'
    ja .done
    sub cl, '0'
    mul r8
    add rax, rcx
    inc rsi
    inc rdi
    jmp .loop
.done:
    mov rdx, rsi
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
; rdi
parse_int:
    mov cl, [rdi]
    cmp cl, '-'
    jne parse_uint
    inc rdi
    call parse_uint
    cmp rdx, 0
    je .done
    inc rdx
    neg rax
.done:
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
; rdi
; rsi
; rdx
string_copy:
    push bx

    xor rax, rax
.loop:
    cmp rax, rdx
    je .overflow
    inc rax
    mov bl, [rdi]
    mov [rsi], bl
    cmp bl, 0
    je .copied
    inc rdi
    inc rsi
    jmp .loop
.overflow:
    xor rax, rax
.copied:

    pop bx
    ret
